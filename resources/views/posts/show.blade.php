@extends('layout.app')

@section('content')
	<h1>{{$posts->title}}</h1>
	<small>Written on {{$posts->created_at}}</small>
	<div>
		{{$posts->body}}
	</div>
@endsection