<nav class="navbar navbar-expand-lg navbar-light bg-dark text-light">
  <a class="navbar-brand" href="#">B179</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-link" href="/">Home</a>
      <a class="nav-link" href="/about">About</a>
      <a class="nav-link" href="/services">Services</a>

      <div class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                Blog
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="/posts">Posts</a>
                <a class="dropdown-item" href="/posts/create">Create</a>
              </div>
            </div>
    </div>
  </div>
</nav>