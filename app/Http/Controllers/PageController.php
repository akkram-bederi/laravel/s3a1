<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello(){
        // return view('hello')->with('name','Akkram')
        $info = array(
            'front_end'=>'Zuitt Coding Bootcamp',
            'topics'=>['HTML', 'CSS','JS','PHP','MySQL']
        );
        return view('hello')->with($info);
    }

    public function index(){
    return view('index');
    }  

    public function about(){
    return view('about');
    } 

    public function services(){
    return view('services');
    }  
}

